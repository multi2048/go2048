BUILDDIR = _build
MKDIR = mkdir -p

.PHONY: default go2048 go2048-run

default: $(BUILDDIR)/go2048

all: go2048

run: go2048-run

$(BUILDDIR)/go2048: go2048.go
	go build -o $@ $<

go2048: $(BUILDDIR)/go2048

go2048-run: go2048.go
	go run $<
