//usr/bin/env go run $0 $@; exit $?
package main

import (
    "fmt"
    "reflect"
    "strings"
    "time"
    "math/rand"
    "github.com/eiannone/keyboard"
)

var board [4][4]int
var status [3]int

func printboard() {
    fmt.Print("\033[2J") // Clear screen
    fmt.Print("\033[2;3HScore:")
    fmt.Print("\033[3;3HTurns:")
    frame := [2][2]string{{"|"," "},{"+","-"}}
    fmt.Printf("\033[%d;5H%s",5,strings.Repeat(frame[1][0] + strings.Repeat(frame[1][1],4),4)+frame[1][0])
    for r := range board {
        for l := 0; l <= 1; l++ {
            fmt.Printf("\033[%d;5H%s",r*2+6+l,strings.Repeat(frame[l][0] + strings.Repeat(frame[l][1],4),4)+frame[l][0])
        }
    }
    fmt.Print("\033[15;3HControls: HJKL, ASWD, ←↓↑→, 4286")
    fmt.Print("\033[16;7HQuit: Q, X, ^Q, ^X, ^C, Esc")
}

func printstate() {
    start := [2]int{6,6}
    for r := range board {
        for c := range board[r] {
            fmt.Printf("\033[%d;%dH%s", r*2+start[0], c*5+start[1], "    ")
            if board[r][c] > 0 {
                fmt.Printf("\033[%d;%dH%d", r*2+start[0], c*5+start[1], board[r][c])
            }
        }
    }
    fmt.Printf("\033[2;10H%d",status[0])
    fmt.Printf("\033[3;10H%d",status[1])
    fmt.Print("\033[18;0H")
}

func addnum() {
    var locs [len(board)*len(board[0])][2]int
    //fill locs with out-of-range values so we can detect them later
    for l := 0; l < len(locs); l++ {
        locs[l][0] = len(board)
        locs[l][1] = len(board[0])
    }
    //Populate locs with board coordinates that are 0
    l := 0
    for r,rd := range board {
        for c,cd := range rd {
            if cd == 0 {
                locs[l][0] = r
                locs[l][1] = c
                l++
            }
        }
    }
    if locs[0][0] < len(board) {
        //Figure out how many zeroes we have
        l = 0
        for lt := 0; lt < len(locs); lt++ {
            if locs[lt][0] < len(board) {
                l++
            }
        }
        //pick a random location from the gathered zero-locations
        l = rand.Intn(l)
        //Place a 2 (90%) or 4 (10%) at said location on the board
        if rand.Intn(9) == 0 {
            board[locs[l][0]][locs[l][1]] = 4
        } else {
            board[locs[l][0]][locs[l][1]] = 2
        }
    }
}

func mirror() {
    var tmpboard [4][4]int
    for r := range board {
        tc := len(board[r])
        for c := range board[r] {
            tc--
            tmpboard[r][tc] = board[r][c]
        }
    }
    board = tmpboard
}

func invert() {
    var tmpboard [4][4]int
    for r := 0; r < len(board); r++ {
        for c := 0; c < len(board[r]); c++ {
            tmpboard[c][r] = board[r][c]
        }
    }
    board = tmpboard
}

func pushlt() {
    var tmpboard [4][4]int
    for r := 0; r < len(board); r++ {
        tc := 0
        combined := false
        for _,cd := range board[r] {
            if cd != 0 {
                if !combined && tc != 0 && tmpboard[r][tc-1] == cd {
                    tmpboard[r][tc-1] = cd*2
                    status[0] += cd*2
                    combined = true
                } else {
                    tmpboard[r][tc] = cd
                    combined = false
                    tc++
                }
            }
        }
    }
    if board != tmpboard {
        status[1]++
        board = tmpboard
        addnum()
    }
}

func pushrt() {
    mirror()
    pushlt()
    mirror()
}

func pushup() {
    invert()
    pushlt()
    invert()
}

func pushdn() {
    invert()
    pushrt()
    invert()
}

func contains(arr interface{}, item interface {}) bool {
    a := reflect.ValueOf(arr)
    if a.Kind() != reflect.Array {
        panic("Invalid data type")
    }
    for i := 0; i < a.Len(); i++ {
        if a.Index(i).Interface() == item {
            return true
        }
    }
    return false
}

func main() {
    rand.Seed(time.Now().UnixNano())

    if err := keyboard.Open(); err != nil {
        panic(err)
    }
    printboard()
    addnum()
    addnum()
    run := true
    for run {
        printstate()
        char, key, err := keyboard.GetKey()
        if err != nil {
            panic(err)
        }
        ch := string(char)
        switch {
            case key==keyboard.KeyArrowLeft,contains([5]string{"h","H","a","A","4"},ch):
                pushlt()
            case key==keyboard.KeyArrowRight,contains([5]string{"l","L","d","D","6"},ch):
                pushrt()
            case key==keyboard.KeyArrowUp,contains([5]string{"k","K","w","W","8"},ch):
                pushup()
            case key==keyboard.KeyArrowDown,contains([5]string{"j","J","s","S","2"},ch):
                pushdn()
            case contains([4]string{"q","Q","x","X"},ch),contains([4]keyboard.Key{keyboard.KeyCtrlX,keyboard.KeyCtrlQ,keyboard.KeyCtrlC,keyboard.KeyEsc},key):
                run = false
        }
    }
    defer func() {
        _ = keyboard.Close()
    }()
}
